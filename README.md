# mrdiff

![](https://img.shields.io/badge/written%20in-C%2B%2B%2C%20Javascript-blue)

A binary-safe diff/merge/patch library for multiple languages.

Implements the longest common subsequence (Myers, 1986) and shortest middle snake functions for diffing text as per http://www.mathertel.de/Diff/DiffDoku.aspx , in both C++ and javascript.

Source code and command-line binary included with download.


## Download

- [⬇️ mrdiff-1.2.7z](dist-archive/mrdiff-1.2.7z) *(55.02 KiB)*
- [⬇️ mrdiff-1.1.7z](dist-archive/mrdiff-1.1.7z) *(22.84 KiB)*
- [⬇️ mrdiff-1.0.7z](dist-archive/mrdiff-1.0.7z) *(19.74 KiB)*
